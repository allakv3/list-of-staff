import React, { Component } from 'react';
import {
  Link
} from 'react-router-dom';
import Modal from '../components/Modal';

class Detail extends Component {
  constructor(props) {
    super(props);
    this.toggleState = this.toggleState.bind(this);
    this.state = {
      isModalOpened: false,
      currentStaff: {}
    };
  }

  getCurrentStaff() {
    let arr = this.props.staff.filter((el) => {
      return el.id == this.props.match.params.id;
    })
    let currentStaff = arr[0]
    this.setState({ currentStaff });
  }

  componentWillMount() {
    this.getCurrentStaff()
  }

  toggleState() {
    this.setState({ isModalOpened: !this.state.isModalOpened })
  }

  render() {
    let modal;
    if(this.state.isModalOpened) {
      modal = <Modal buttonClick={this.toggleState} editStaff={this.props.editStaff} item={this.state.currentStaff} id={this.props.match.params.id} title={'Редактирование'}/>
    }

    return (
      <div className="container">
        <Link to="/" className="btn btn-outline-secondary mb-4 mt-2">К списку</Link>
        <h1 className="h1 mb-5">Информация о сотруднике</h1>
        <div className="mb-4">
          <p><strong>Имя: </strong>{this.state.currentStaff.name || 'нет данных'}</p>
          <p><strong>Фамилия: </strong>{this.state.currentStaff.sername || 'нет данных'}</p>
          <p><strong>Должность: </strong>{this.state.currentStaff.position || 'нет данных'}</p>
          <p><strong>Описание: </strong>{this.state.currentStaff.description || 'нет данных'}</p>
        </div>
        <button
          type="button"
          className="btn btn-primary mb-4"
          onClick={this.toggleState}
        >Редактировать</button>
        {modal}
      </div>
    );
  }
}

export default Detail;