import React, { Component } from 'react';
import Modal from '../../components/Modal';
import Table from './Table';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = { isModalOpened: false };
    this.toggleState = this.toggleState.bind(this)
  }

  toggleState() {
    this.setState({ isModalOpened: !this.state.isModalOpened })
  }

  render() {
    let modal;
    if (this.state.isModalOpened) {
      modal = <Modal buttonClick={this.toggleState} addStaff={this.props.addStaff} title={'Добавление сотрудника'} />
    }
    return (
      <div className="App container">
        <h1 className="h1 mb-4">Список сотрудников</h1>
        <button
          type="button"
          className="btn btn-primary mb-4"
          onClick={this.toggleState}
        >Добавить</button>
        <Table staff={this.props.staff}/>
        {modal}
      </div>
    );
  }
}

export default Main;
