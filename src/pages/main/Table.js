import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';


class Table extends Component {
  constructor(props) {
    super(props);
    this.state={};
  }

  goTo = (id) => () => this.props.history.push(`/staff/${id}`);

  render() {
    const list = this.props.staff.map((item) => (
      <tr key={item.id} onClick={this.goTo(item.id)}>
        <td>{item.name}</td>
        <td>{item.sername}</td>
        <td>{item.position}</td>
      </tr>
    ));

    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">Имя</th>
            <th scope="col">Фамилия</th>
            <th scope="col">Должность</th>
          </tr>
        </thead>
        <tbody>
          {list}
        </tbody>
      </table>
    )
  }
}

export default withRouter(Table);