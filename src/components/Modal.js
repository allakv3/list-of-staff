import React, { Component } from 'react';

class Modal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.item ? this.props.item.name : '',
      sername: this.props.item ? this.props.item.sername : '',
      position: this.props.item ? this.props.item.position : '',
      description: this.props.item ? this.props.item.description : ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    if (this.props.item) {
      this.props.editStaff(this.state, this.props.id)
    } else {
      this.props.addStaff(this.state)
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <div className="modal" tabIndex="-1" role="dialog">
        <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{this.props.title}</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={this.props.buttonClick}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label htmlFor="name">Имя</label>
                <input
                  type="text"
                  name="name"
                  value={this.state.name}
                  onChange={this.handleChange}
                  className="form-control"
                  d="name"
                />
              </div>
              <div className="form-group">
                <label htmlFor="sername">Фамилия</label>
                <input
                  type="text"
                  name="sername"
                  value={this.state.sername}
                  onChange={this.handleChange}
                  className="form-control"
                  id="sername"
                />
              </div>
              <div className="form-group">
                <label htmlFor="position">Должность</label>
                <input
                  type="text"
                  name="position"
                  value={this.state.position}
                  onChange={this.handleChange}
                  className="form-control"
                  id="position"
                />
              </div>
              <div className="form-group">
                <label htmlFor="description">Описание</label>
                <textarea
                  className="form-control"
                  name="description"
                  value={this.state.description}
                  onChange={this.handleChange}
                  id="description"
                  rows="3"
                />
              </div>
              <div className="text-center">
                <button className="btn btn-primary">Сохранить</button>
              </div>
            </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Modal;