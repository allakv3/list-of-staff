import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
  HashRouter,
  Route,
} from 'react-router-dom';
import './index.css';
import Main from './pages/main';
import Detail from './pages/Detail';
import * as serviceWorker from './serviceWorker';

let DEFAULT_STAFF = [
  {
    id: 43342,
    name: 'Mark',
    sername: 'Otto',
    position: 'Manager',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
  },
  {
    id: 56423,
    name: 'Jacob',
    sername: 'Thornton',
    position: 'Manager',
    description: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?'
  }
]

class App extends Component {
  constructor(props) {
    super(props);
    this.addStaff = this.addStaff.bind(this);
    this.editStaff = this.editStaff.bind(this);
  }

  addStaff(data) {
    data.id = new Date().getTime();
    const staff = [...this.state.staff, data];
    localStorage.setItem('staff', JSON.stringify(staff));
    this.setState({ staff });
  }

  editStaff(data, id) {
    data.id = id;
    const newStaff = this.state.staff.map((el) => {
      if (el.id == data.id) {
        return data;
      } else {
        return el;
      }
    })
    localStorage.setItem('staff', JSON.stringify(newStaff));
    this.setState({ staff: newStaff });
  }

  componentWillMount() {
    let staff;
    const storedStaff = localStorage.getItem('staff')
    if (storedStaff) {
      staff = JSON.parse(storedStaff);
    } else {
      localStorage.setItem('staff', JSON.stringify(DEFAULT_STAFF));
      staff = DEFAULT_STAFF;
    }
    this.setState({ staff });
  }

  render() {
    return (
      <HashRouter>
        <div>
          <Route
            exact
            path="/"
            component={
              props => <Main {...props} staff={this.state.staff} addStaff={this.addStaff} />
            }
          />
          <Route
            exact
            path="/staff/:id"
            component={
              props => <Detail {...props} staff={this.state.staff} editStaff={this.editStaff}/>
            }
          />
        </div>
      </HashRouter >
    )
  }
}

ReactDOM.render(
  <App/>,
  document.getElementById('root')
);

serviceWorker.unregister();
